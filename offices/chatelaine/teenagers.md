---
title: Teenagers
---
<p>One group that is particularly challenging to deal with is teenagers<br />
        whose parents are not in the SCA. They are too young to fight or marshal,<br />
        yet too old for "children's activities". Often, parents are concerned about<br />
        the type of group that their son/daughter is getting involved with. Here are<br />
        a few suggestions that can help to keep all concerned happy: parents,<br />
        teenagers, and the SCA members.</p>
<ol>
<li>Invite the teenager's parents to join him/her. Make sure the parents<br />
          know they can visit any time they want, even if they choose not to join.<br />
          This lets the parent know there is nothing secretive going on with their<br />
          child.</li>
<li>Have a meeting, perhaps at a member's house, to explain the SCA to the<br />
          teenager and the parents. Stress the educational aspects and the things<br />
          that the teenager can do.</li>
<li>Make a packet for teenagers and their parents. Include the rules for<br />
          minors, and a list of things that they can and cannot do, and a written<br />
          invitation to the parents to come join in the fun. You may also include<br />
          the things listed in the early section on new members packets.</li>
<li>Make sure you know all the rules pertaining to minors: waivers needed,<br />
          medical release forms, age restrictions for activities, etc. Be sure the<br />
          parents know these rules, also.</li>
<li>Never break the rules to keep the teenager interested. Under-aged<br />
          teenagers cannot fight at any event or on any property that is being<br />
          rented, leased, or borrowed in the name of the SCA, in Drachenwald. No<br />
          exceptions. If children do not want to participate unless they can do<br />
          restricted activities, inform them that you look forward to their return<br />
          to the SCA when they come of age.</li>
<li>Introduce the teenager to other teenagers who are functioning well in<br />
          the SCA. Also, introduce the teenager to an adult with similar<br />
          interests.</li>
<li>Form a guild specially for people in this age range. For example, a<br />
          drum corps could be formed to lead troops to battle. (Of course they must<br />
          leave the field when the battle begins.) A teen choir could be organised<br />
          for feast entertainment (this obviously depends on the number and<br />
          willingness of the members involved!) Teenagers could be in charge of<br />
          organising a mid-day snack for the King and Queen.</li>
<li>Have things for the teenagers to do at events. Then, ask them to<br />
          participate. (They may need a bit of encouraging.) For examples, they<br />
          can:<br />
          - help run the children's activities;
<p>          - help serve feast;<br />
          - be a page;<br />
          - help cook a meal;<br />
          - play games such as bocce;<br />
          - be a water bearer (with supervision);<br />
          - participate in arts and sciences activities;</p>
<p>          - attend classes;<br />
          - help decorate/set-up before an event;<br />
          - help clean-up after an event;<br />
          - participate in a quest;<br />
          - entertain at a feast.<br />
          Older teenagers can:</p>
<p>          - fight or practice fighting or authorise;<br />
          - be a scout;<br />
          - be a water bearer (without supervision);<br />
          - be a target archer;<br />
          - be a combat archer;<br />
          - help with crowd control at a demo.</p></li>
</ol>
<p>        The rules for older teenagers participation do occassionally change, so<br />
        it is best to check with the local or Kingdom officer in charge of the<br />
        activity, before the teenager starts any of the restricted activities.</p>
<p>        Please remember: many of these restrictions vary from kingdom to kingdom.<br />
        You must follow the rules of the kingdom which you are in, as well as the<br />
        rules of Drachenwald. If the rules conflict, follow the stricter rule, or<br />
        talk to someone in charge at the event or activity.</p>
