---
title: Public Relations
---
<p><strong>WHY PUBLIC RELATIONS IS IMPORTANT</strong></p>
<p>        The Society does not exist in a vacuum; it must function in the real<br />
        world. The Society functions best when the community understands what the<br />
        SCA is (and is not). The renting of event sites and equipment becomes much<br />
        easier. Recruiting becomes easier. Often, SCA members' non-SCA lives become<br />
        a bit easier. In essence, the SCA needs the acceptance of any community in<br />
        which it is to exist.</p>
<p>        <strong>POSITIVE PUBLIC RELATIONS IDEAS</strong></p>
<p>        There are two basic goals of positive public relations activities; to<br />
        help the people of the local community 1. to understand what the SCA is (and<br />
        is not), and 2. to accept the group as responsible and acceptable. These are<br />
        a few ideas for reaching these goals in your group:</p>
<ol>
<li>(Not sure if this is possible in Europe, but...) Adopt a motorway.<br />
          Often, the sponsoring group will have it's name posted along the motorway,<br />
          (Be sure to list the Society's name, not just "SCA", and the real name of<br />
          the city, not the shire/barony name.)</li>
<li>Donate feast leftovers to local charity.</li>
<li>Do period Christmas caroling for hospital or nursing home,</li>
<li>Do demonstrations for "family night" at the Boy Scouts, Girl Guides,<br />
          etc.</li>
<li>Use high visibility locations for many of the things that your group<br />
          does weekly: fighter practice, dance practice, populace meetings, etc.<br />
          This can help in recognition of the group.</li>
<li>Find out what is going on in your community and get involved.</li>
<li>Put up a complimentary subscription of Tournaments Illuminated or a<br />
          copy of The Known World Handbook in the local library. Or donate some<br />
          period books. You might even ask the library to have a newspaper<br />
          photographer present when the books are presented, and do the presentation<br />
          in garb. Library subscriptions to T.I. are cheaper than memberships, and<br />
          can be combined with the Dragon's Tale. This does not include a membership<br />
          card.</li>
<li>Put window displays in windows of the business district.</li>
<li>Put an art/science display in the local museum.</li>
<li>Put the SCA in the telephone book. Your group should list a contact<br />
          number in the phone book only if you are sure that the number will not<br />
          change for a couple of years. A non-political home-owner is a good<br />
          choice.</li>
<li>Donate time working at the public television (or radio) telethon.</li>
<li>Attend period plays in garb, or volunteer to act as ushers in garb.<br />
          (Make sure to contact the theatre well in advance of the<br />
          performance!)</li>
<li>At the beginning of the school year, take black and white photographs<br />
          to a student newspaper office, along with a short written story or a<br />
          flier. Be sure to leave the name and phone number of a contact<br />
          person.</li>
<li>If your group has money, sponsor a programme on public radio or<br />
          television, e.g. a Celtic music or Medieval music programme.</li>
<li>If there is an association shield board at the edge of town (where the<br />
          Rotary, Lions , or others advertise their presence,) you may be able to<br />
          list our branch there. You must have a listing in the telephone book to go<br />
          with it.</li>
</ol>
<p>        Whatever you choose to do, remember to tailor your activity to the<br />
        situation and the audience.</p>
<p>        You may have noticed, some of these things that promote good public<br />
        relations are also things that can be used as recruiting. In fact, most of<br />
        the listed ideas can act as both recruiting and public relations, to varying<br />
        degrees.</p>
<p>        Another way to improve the way that the public sees the SCA is to connect<br />
        it (in the minds of the public) to another well-known and respected<br />
        institution. You might decide to do the "theatre" part of a dinner-theatre<br />
        at a local hotel/restaurant. Whatever you decide to do, the publicity for<br />
        the event should include both the names of the SCA and the other group.</p>
