---
title: Kingdom Chronicler
featureimg: images/foo/test.jpg
featurealt: thing
---
<p>Here you will find information about the office of the Kingdom Chronicler and the Kingdom newsletter - the Dragon's Tale; how to write for the newsletter; how to use the Kingdom Calendar and write announcements; and guidelines for local chroniclers.</p>

<p>Update 2016-05-27<br />There are new ways of reading <em>Dragon's Tale online</em>.</p>

<p>Go to <a href="https://members.sca.org/apps/#Newsletters" target="_blank">https://members.sca.org/apps/#Newsletters</a> and log in with the same login information that you use to renew your membership.</p>

<p>If you are member of an affiliation (for example Nordmark or Aarnimetsä) and don't have an SCA inc membership number you should log in with the username "affiliate" (all lowercase) and password "affiliate" (also all lowercase). There is even an option to "remember me on this computer", so you don't have to enter in your login information each time! The nuances of logging in are on the left side of the screen, including methods for getting help.

<p>Once you are logged in: at the top of the screen are the membership options. Click the button for newsletter access on the far right. This will open the directory of Kingdoms. Note that on the directory of Kingdoms, there is a folder marked Archives. This is where the older issues of the electronic newsletters are maintained. These files currently go back to March 2012 when we first introduced electronic newsletters.</p>

<p>If you have any difficulties or questions, please email <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,109,101,109,98,101,114,115,104,105,112,64,115,99,97,46,111,114,103,39,62,109,101,109,98,101,114,115,104,105,112,64,115,99,97,46,111,114,103,60,47,97,62));</script>. Include your modern name, membership number, and a brief explanation of the problem you're experiencing.</p>

<h3><a name="about"></a>About the Kingdom Chronicler</h3>

<p>The Chronicler is responsible for all official Kingdom publications, in particular, the Kingdom Newsletter - the <em>Dragon's Tale</em>.</p>

<h3><a name="newsletter"></a>Kingdom Newsletter</h3>

<ul>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/kingdom-newsletter.md %}">The Dragon's Tale</a>
<ul>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/kingdom-newsletter.md %}#dt">Submitting Material to the Dragon's Tale</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/kingdom-newsletter.md %}#ads">Advertising in the Dragon's Tale</a></li>
</ul>
</li>
</ul>

<h3><a name="policies"></a>Policies and Reports</h3>

<ul>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/chronicler-policies.md %}">Kingdom Chronicler Policies</a> </li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/report-template.md %}">Template for Quarterly Reports</a></li>
</ul>

<h3><a name="local"></a>Local Chroniclers</h3>

<p>Local branches are encouraged to have a newsletter, though it is only <em>required</em>for Baronies and above.</p>

<ul>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/guidelines-local-chroniclers.md %}">Guidelines for Local Chroniclers</a></li>
<li><a href="http://www.sca.org/officers/chronicler/blackfox-awards.html">William Blackfox Awards</a><br />The William Blackfox awards are given each year to recognise excellence in local newsletters and their contributors. Each year, the Kingdom Chronicler nominates local newsletters, articles and artwork produced within the Kingdom for the various awards, so make sure you always send a copy (as required by policy). For more details on the awards, follow the link above.</li>
</ul>

<h3>Release Forms</h3>

<p>Release forms are needed for original articles and artwork, and from persons appearing on photographs. Please see the Release Forms page for further details</p>

<ul>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/release-forms.md %}">Release Forms</a></li>
</ul>

<h3><a name="calendar"></a>Event Announcements</h3>

<p>In order for an event to be "official for business", i.e. for things like awards, proclamation of laws etc to take place, the event must be properly scheduled on the Kingdom Calendar and an event published in the Dragon's Tale.</p>

<ul>
<!-- <li><a href="/content/planning-and-scheduling-events">Planning and scheduling events</a> FIXME</li> -->
<li><a href="{{ site.baseurl }}{% link events/calendar-add.html %}" target="_blank">Request a date on the Kingdom Calendar</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/guidelines-event-announcement.md %}">Event announcement guidelines</a></li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/make-event-official.md %}">Make sure your event is official</a></li>
</ul>

{% include officer-contacts.html %}